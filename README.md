import hashlib
import hmac

access_token = "{access-token}"
app_secret = "{app-secret}"

appsecret_proof = hmac.new(app_secret.encode(), msg=access_token.encode(), digestmod=hashlib.sha256).hexdigest()
print(appsecret_proof)
